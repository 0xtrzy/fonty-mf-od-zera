PLIK = lekcja1
LATEX = lualatex

$(PLIK).dvi: $(PLIK).tex
	$(LATEX) $<

Testowy12.tfm: testowy.mf Testowy12.mf
#	mf '\mode=ljfour; mag:=1; nonstopmode; input Testowy12.mf'
	mf Testowy12.mf
	gftopk -verbose Testowy12.2602gf  Testowy12.600pk

clean:
	rm -f $(PLIK).pdf $(PLIK).log $(PLIK).aux $(PLIK).dvi *.log
	rm -f *.600gf *.2602gf *.600pk *.tfm
